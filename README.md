# gdfs 分布式文件系统

#### 介绍
gdfs分布式文件服务基于GO语言，前端页面使用vue-admin，主要用于小型文件存储。

它使用了带权重RoundRobin算法实现的负载均衡,使用技术框架gin+jwt+viper+gorm...等.

#### 软件架构

网关和存储服务关系，如下图所示。 网关下一级是节点，每个节点下可以有N个服务，服务之间数据互相同步，互相备份。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/170052_eca3d8a8_4814434.png "gdfs架构.png")


#### 网关服务介绍:   

主要功能             
1. http请求转发
2. TCP服务端
3. 收集存储服务信息
4. 提供前端页面接口
5. 数据迁移

地址: https://gitee.com/llakcs/gdfs-center

#### 文件存储服务介绍:   
主要功能 
1.  文件下载
2.  文件存储
3.  批量删除文件
4.  文件更新
5.  增量同步文件
6.  TCP客户端

地址: https://gitee.com/llakcs/gdfs-service


#### 配置文件: config.yml
//数据库配置
gorm:

  username: ${DATASOURCE_USERNAME:root}

  password: ${DATASOURCE_PASSWORD:3100386a}

  dbname: ${FILE_DB_NAME:gdfs-center}

  address: ${DATASOURCE_HOST:127.0.0.1}

  dbport: ${DATASOURCE_PORT:3306}


//文件存储位置

storagePath:

  linuxStorage: ./fileData/tenfineFiles/

//web启动配置

web:

  port: ${FILE_SERVER_PORT:8878}

//tcp端口

tcptun:

  port: ${TCPTUN:5566}

//日志配置

log:

  path: ./fileData/fileServicelog/

//存储服务注册网关配置

gdfs:

  registerConfig:    

    port: ${REGPORT:5566}         //端口

    IpAddr: ${REGIP:127.0.0.1}  //IP

    weight: "1"   //权重
#### 支持协议

1.http
2.tcp 需要自己修改协议

#### 使用说明

1.  打开网关服务代码 go get 更新依赖
2.  进入网关服务代码根目录修改config.yml配置后运行程序
3.  进入网关服务代码找到web目录下gdfs-vue-admin改好vue相关配置 进入根目录 npm run dev 进入前端页面如下图所示 默认账号admin 密码123456
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/212523_847e0a96_4814434.png "企业微信截图_16354275049433.png")
4.  点击登录后，进入系统后台 找到服务管理和节点管理
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/212840_78a0ac1a_4814434.png "企业微信截图_1635427650694.png")
5.  先点开节点管理选择创建节点，如下图所示
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/213234_19b13263_4814434.png "企业微信截图_16354279271960.png")
6.  运行文件存储服务，再去前端服务管理页面找到文件服务点击绑定，把相应的服务绑到对应的节点上，如下图所示。
![输入图片说明](https://images.gitee.com/uploads/images/2021/1028/213806_8f64c1af_4814434.png "企业微信截图_16354282744941.png")
7. 全量迁移数据：应用场景单对单，即单个文件服务把现有数据拷贝到新的文件服务上,不管是文件还是数据库表数据。


# License

MIT License