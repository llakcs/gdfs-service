package controller

import (
	"crypto/md5"
	"encoding/hex"
	"gdfs-service/conf"
	"gdfs-service/constant"
	"gdfs-service/controller/vo"
	"gdfs-service/db"
	"gdfs-service/logcat"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

// @Summary 更新文件接口
// @Description 更新文件接口
// @Accept multipart/form-data
// @Param files formData  string true "要上传的文件"
// @Param flag formData  string false "标志"
// @Param taskId formData  string false "任务id"
// @Param fileKey formData  string false "文件key"
// @Tags 文件上传下载接口
// @Success 200 {object} Result
// @Router /v1/fileUpdate [post]
func UpdateFileByKey(c *gin.Context) {
	c.Writer.Header().Set("content-type", "application/json")
	var uploadvo vo.UploadVo
	result := vo.NewResultInstance()
	err := c.Request.ParseMultipartForm(30)
	if err != nil {
		logcat.Info(err)
		return
	}
	updatekey := c.Request.FormValue("fileKey")
	if updatekey == "" {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.NOTVALID_ERROR], constant.NOTVALID_ERROR, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	//查询需要更新的key是否存在
	var fileInfo db.FileInfo
	db.GetDB().Where("file_key = ?", updatekey).First(&fileInfo)
	if fileInfo == (db.FileInfo{}) {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.UNFIND_FILEKEY_ERROR], constant.UNFIND_FILEKEY_ERROR, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	//找到秘钥去删除物理文件
	s := strings.Split(fileInfo.FilePath, "/")
	filepath := conf.GetStorePath() + s[len(s)-2] + "/" + fileInfo.FilePath + "/" + fileInfo.FileName
	log.Println("###删除路径：", filepath)
	removeer := os.Remove(filepath)
	if removeer != nil {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.REMOVE_FILE_ERR], constant.REMOVE_FILE_ERR, removeer)
		WriteString(c.Writer, result)
		return
	}
	srcfile, srcfileheader, err := c.Request.FormFile("files") // file 是上传表单域的名字
	if err != nil {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.NOTVALID_ERROR], constant.NOTVALID_ERROR, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	flag := c.Request.FormValue("flag")
	taskId := c.Request.FormValue("taskId")
	logcat.Info("#########----taskId", taskId, "// flag =", flag)
	if srcfile == nil || srcfileheader.Size == 0 {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.NOTVALID_ERROR], constant.NOTVALID_ERROR, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	defer srcfile.Close()
	var filemaxsize = 30720
	logcat.Info("####文件大小:", srcfileheader.Size)
	if filemaxsize < int(srcfileheader.Size)/1024 {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.UPLOADFILE_SIZE_ERROR], constant.UPLOADFILE_SIZE_ERROR, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	//获取文件后缀名位置
	index := strings.LastIndex(srcfileheader.Filename, ".")
	//获取文件后缀名
	subfix := srcfileheader.Filename[index:]
	//生成本地存储的文件名
	//localFileName := fmt.Sprintf("%d", time.Now().Unix()) + subfix
	localFileName := strconv.FormatInt(time.Now().UnixNano(), 10) + CreateRandomString(8) + subfix
	date := time.Now().Format("2006-01-02")
	storagePath := conf.GetStorePath() + date + "/"
	_, fileErr := os.Stat(storagePath)
	if os.IsNotExist(fileErr) {
		//文件夹不存在创建
		createErr := os.Mkdir(storagePath, os.ModePerm)
		if createErr != nil {
			c.String(http.StatusBadRequest, "保存失败 Error:%s", err.Error())
		}
	}
	if err = c.SaveUploadedFile(srcfileheader, storagePath+localFileName); err != nil {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.SAVEFILEFAIL], constant.SAVEFILEFAIL, err)
		WriteString(c.Writer, result)
		return
	}
	logcat.Info("文件路径:", storagePath+localFileName)
	fi, errr := os.Stat(storagePath + localFileName)
	if errr != nil {
		result.Fail(constant.BussinessErrorType[constant.SAVEFILEFAIL], "打开文件详细信息失败", uploadvo)
		WriteString(c.Writer, result)
		return
	}
	sgin := md5.Sum([]byte(localFileName))
	filekey := hex.EncodeToString(sgin[:])
	//使用真随机生成字符串秘钥
	//randomkey := CreateRandomString(64)
	////使用sha256加密
	//filekeysum := sha256.Sum256([]byte(randomkey))
	//filekey := hex.EncodeToString(filekeysum[:])
	logcat.Info("#########--------上传你文件生成文件秘钥:", filekey)
	fileInfo.FileKey = filekey
	fileInfo.FileOriginalName = srcfileheader.Filename
	logcat.Info("####原文件名:", srcfileheader.Filename, "本地文件名:", localFileName)
	fileInfo.FileName = localFileName
	fileInfo.FilePath = "/" + date + "/"
	fileInfo.FileSize = strconv.FormatInt(fi.Size(), 10)
	fileInfo.FileSuffixName = subfix
	//存入数据库
	dberr := db.GetDB().Save(&fileInfo).Error
	if dberr != nil {
		//存入数据失败
		logcat.Info("####文件上传失败:", dberr)
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.SAVEFILEFAIL], constant.SAVEFILEFAIL, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	logcat.Info("#########--------数据库中的文件秘钥:", fileInfo.FileKey, "储存的文件名称:", fileInfo.FileName)
	uploadvo.FileKey = fileInfo.FileKey
	uploadvo.Flag = flag
	uploadvo.TaskId = taskId
	uploadvo.UploadSize = fileInfo.FileSize
	result.SucessDefault(uploadvo)
	WriteString(c.Writer, result)
	return
}
