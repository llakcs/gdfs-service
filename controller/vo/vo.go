package vo

type DownloadVo struct {
	FileOriginalName string `json:"fileOriginalName"`
	Filesize         string `json:"filesize"`
	FileKey          string `json:"fileKey"`
	FileBase64       string `json:"fileBase64"`
	FileData         []byte `json:"fileData"`
}

type UploadVo struct {
	FileKey    string `json:"fileKey"`
	TaskId     string `json:"taskId"`
	Flag       string `json:"flag"`
	UploadSize string `json:"uploadSize"`
}

type BatchDeletVo struct {
	Sucesslist Sucesslist `json:"sucesslist"`
	Faillist   Faillist   `json:"faillist"`
}

type Sucesslist struct {
	Filekey []FilekeyInfo `json:"filekeyinfo"`
}

type Faillist struct {
	Filekey []FilekeyInfo `json:"filekeyinfo"`
}

func (f *Faillist) Add(info FilekeyInfo) {
	f.Filekey = append(f.Filekey, info)
}

func (s *Sucesslist) Add(f FilekeyInfo) {
	s.Filekey = append(s.Filekey, f)
}

type FilekeyInfo struct {
	Filekey     string `json:"filekey"`
	Filename    string `json:"filename"`
	Filemessage string `json:"filemessage"`
}
