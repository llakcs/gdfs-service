package controller

import (
	"gdfs-service/conf"
	"gdfs-service/constant"
	"gdfs-service/controller/vo"
	"gdfs-service/db"
	"gdfs-service/logcat"
	"github.com/gin-gonic/gin"
	"os"
	"strconv"
)

func RecvSyncFile(c *gin.Context) {
	c.Writer.Header().Set("content-type", "application/json")
	var uploadvo vo.UploadVo
	result := vo.NewResultInstance()
	err := c.Request.ParseMultipartForm(30)
	if err != nil {
		logcat.Info(err)
		return
	}
	srcfile, srcfileheader, err := c.Request.FormFile("files") // file 是上传表单域的名字
	if err != nil {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.NOTVALID_ERROR], constant.NOTVALID_ERROR, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	fileKey := c.Request.FormValue("fileKey")
	fileName := c.Request.FormValue("fileName")
	fileOriginalName := c.Request.FormValue("fileOriginalName")
	content := c.Request.FormValue("content")
	fileSuffixName := c.Request.FormValue("fileSuffixName")
	if srcfile == nil || srcfileheader.Size == 0 {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.NOTVALID_ERROR], constant.NOTVALID_ERROR, uploadvo)
		WriteString(c.Writer, result)
		return
	}
	defer srcfile.Close()
	storagePath := conf.GetStorePath() + content + "/"
	_, fileErr := os.Stat(storagePath)
	if os.IsNotExist(fileErr) {
		//文件夹不存在创建
		createErr := os.Mkdir(storagePath, os.ModePerm)
		if createErr != nil {
			logcat.Info("###创建文件异常:", createErr)
			result.Fail(constant.BussinessErrorType[constant.SAVEFILEFAIL], constant.SAVEFILEFAIL, createErr)
			WriteString(c.Writer, result)
			return
		}
	}
	if err = c.SaveUploadedFile(srcfileheader, storagePath+fileName); err != nil {
		uploadvo.FileKey = ""
		uploadvo.Flag = ""
		uploadvo.TaskId = ""
		result.Fail(constant.BussinessErrorType[constant.SAVEFILEFAIL], constant.SAVEFILEFAIL, err)
		WriteString(c.Writer, result)
		return
	}
	fi, errr := os.Stat(storagePath + fileName)
	if errr != nil {
		result.Fail(constant.BussinessErrorType[constant.SAVEFILEFAIL], "打开文件详细信息失败", uploadvo)
		WriteString(c.Writer, result)
		return
	}
	var fileInfo db.FileInfo
	db.GetDB().Where("file_key = ?", fileKey).First(&fileInfo)
	if fileInfo == (db.FileInfo{}) {
		fileInfo.FileKey = fileKey
		fileInfo.FileOriginalName = fileOriginalName
		fileInfo.FileName = fileName
		fileInfo.FilePath = "/" + content + "/"
		fileInfo.FileSize = strconv.FormatInt(fi.Size(), 10)
		fileInfo.FileSuffixName = fileSuffixName
		fileInfo.SyncStatus = "Y"
		//存入数据库
		dberr := db.GetDB().Create(&fileInfo).Error
		if dberr != nil {
			//存入数据失败
			logcat.Info("####文件上传失败:", dberr)
			uploadvo.FileKey = ""
			uploadvo.Flag = ""
			uploadvo.TaskId = "'"
			result.Fail(constant.BussinessErrorType[constant.SAVEFILEFAIL], constant.SAVEFILEFAIL, uploadvo)
			WriteString(c.Writer, result)
			return
		}
	} else {
		logcat.Info("##文件已存在")
	}
	uploadvo.FileKey = fileInfo.FileKey
	uploadvo.UploadSize = fileInfo.FileSize
	result.SucessDefault(uploadvo)
	WriteString(c.Writer, result)
}
