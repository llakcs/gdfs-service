package utils

import (
	"fmt"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/disk"
	"github.com/shirou/gopsutil/mem"
	"strconv"
	"time"
)

//CPU使用率和物理核心和主频
func GetCpuInfo() (float64, int, float64) {
	percent, _ := cpu.Percent(time.Second, false)
	count, _ := cpu.Counts(false) //cpu物理核心
	infos, _ := cpu.Info()
	return Decimal(percent[0]), count, Decimal(infos[0].Mhz / 1024)
}

func Decimal(value float64) float64 {
	value, _ = strconv.ParseFloat(fmt.Sprintf("%.2f", value), 64)
	return value
}

//返回内存使用率 ,内存总数，内存已使用
func GetMemInfo() (float64, uint64, uint64) {
	memInfo, _ := mem.VirtualMemory()
	return Decimal(memInfo.UsedPercent), memInfo.Total / 1024 / 1024 / 1024, memInfo.Used / 1024 / 1024 / 1024
}

//swap
func GetSwapInfo() (float64, uint64, uint64) {
	swapinfo, _ := mem.SwapMemory()
	return Decimal(swapinfo.UsedPercent), swapinfo.Total / 1024 / 1024 / 1024, swapinfo.Used / 1024 / 1024 / 1024
}

//磁盘使用率,磁盘总容量，磁盘已使用
func GetDiskInfo() (float64, uint64, uint64) {
	parts, _ := disk.Partitions(true)
	diskInfo, _ := disk.Usage(parts[0].Mountpoint)
	return Decimal(diskInfo.UsedPercent), diskInfo.Total / 1024 / 1024 / 1024, diskInfo.Used / 1024 / 1024 / 1024
}
