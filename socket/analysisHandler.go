package socket

import (
	"encoding/hex"
	"encoding/json"
	"gdfs-service/conf"
	"gdfs-service/db"
	"gdfs-service/logcat"
	"log"
	"os"
)

var filetmp = make([]byte, 0)
var packNo = 0

//停止同步指令解析
func AnalysisStopFullDataSync(data *DataDockPackage) *Response {
	StopFUllSyncChan <- true
	return GetResponse(data.Cmd, data.SerialNum, "0", "处理成功")
}

//解析通知绑定或解绑节点命令
func AnalysisNoticeNode(data *DataDockPackage) {
	var n NoticeNodeConf
	hexbyte, _ := hex.DecodeString(data.Data)
	jsonerr := json.Unmarshal(hexbyte, &n)
	if jsonerr != nil {
		log.Println("###AnalysisDbInfo json err:", jsonerr)
	}
	//更新到数据库
	var sinfo db.StoreInfo
	db.GetDB().First(&sinfo)
	sinfo.NodeID = n.NodeID
	db.GetDB().Save(&sinfo)
	SetNodeConf(n.NodeID, n.NoticeStores)
}

//解析数据传输指令
func AnalysisDbInfo(data *DataDockPackage) {
	var sdb SyncDb
	hexbyte, _ := hex.DecodeString(data.Data)
	jsonerr := json.Unmarshal(hexbyte, &sdb)
	if jsonerr != nil {
		log.Println("###AnalysisDbInfo json err:", jsonerr)
	}
	d := sdb.Data
	storagePath := conf.GetStorePath() + d.Content + "/"
	_, fileErr := os.Stat(storagePath)
	if os.IsNotExist(fileErr) {
		//文件夹不存在创建
		createErr := os.MkdirAll(storagePath, os.ModePerm)
		if createErr != nil {
			logcat.Info("##文件夹创建失败:", createErr)
			return
		}
	}
	//创建文件
	_, newfileErr := os.Stat(storagePath + d.FileName)
	if os.IsNotExist(newfileErr) {
		//文件不存在
		file, err := os.Create(storagePath + d.FileName)
		defer file.Close()
		if err != nil {
			logcat.Info("##创建文件失败:", err)
			return
		}
		//超过5M的文件进行分割接收
		if d.Pack != 1 {
			filetmp = append(filetmp, d.FileByte...)
			//文件获取完成
			if d.PackNo != d.Pack {
				return
			}
			//写入文件
			n, writeErr := file.Write(filetmp)
			filetmp = filetmp[0:0]
			if writeErr != nil || n == 0 {
				logcat.Info("##写入文件失败:", writeErr)
				return
			}
		} else {
			//正常接收
			n, writeErr := file.Write(d.FileByte)
			if writeErr != nil || n == 0 {
				logcat.Info("##写入文件失败:", writeErr)
				return
			}
		}
	}
	//通过通道发送数据库数据进行批量处理
	SyncDbChan <- sdb
}

//解析连接Tcp服务指令
func AnalysisConnTcpServer(data *DataDockPackage) {
	logcat.Info("###收到连接TCP服务指令---------------------!")
	var connCmd ConnTcpServerCmd
	hexbyte, _ := hex.DecodeString(data.Data)
	jsonerr := json.Unmarshal(hexbyte, &connCmd)
	if jsonerr != nil {
		log.Println("###AnalysisConnTcpServer json err:", jsonerr)
	}
	//通过通道发送连接内容
	ConnTcpChan <- connCmd
}

//解析创建tcp服务指令
func AnalysisCreateTcpServer(data *DataDockPackage) {
	logcat.Info("###收到创建TCP服务指令")
	var serverCmd TranmitServerCmd
	hexbyte, _ := hex.DecodeString(data.Data)
	jsonerr := json.Unmarshal(hexbyte, &serverCmd)
	if jsonerr != nil {
		log.Println("###AnalysisCreateTcpServer json err:", jsonerr)
	}
	CreateTcpServerChan <- serverCmd
}

//解析关闭TCP通道
func AnalysisCloseTcpTun(data *DataDockPackage) {
	var closecmd CloseTcpTun
	hexbyte, _ := hex.DecodeString(data.Data)
	jsonerr := json.Unmarshal(hexbyte, &closecmd)
	if jsonerr != nil {
		log.Println("###AnalysisCloseTcpTun json err:", jsonerr)
	}
	//如果是源数据服务
	if conf.GetStoreid() == closecmd.StoreId {
		StopConnTcpChan <- true
	}
}
