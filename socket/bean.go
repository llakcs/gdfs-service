package socket

import (
	"encoding/json"
	"strconv"
)

//解码struct
type DataDockPackage struct {
	SerialNum uint64 //帧流水号
	Version   uint64 //协议版本
	Cmd       string //命令
	Data      string //数据
	Sum       uint64 //校验和
}

func NewDataDpckPackage(serialNum, version, sum uint64, cmd, data string) *DataDockPackage {
	return &DataDockPackage{
		SerialNum: serialNum,
		Version:   version,
		Sum:       sum,
		Cmd:       cmd,
		Data:      data,
	}
}

type HeartBeatResp struct {
	NodeId string `json:"nodeId"`
}

//心跳
type HeartBeatInfo struct {
	ID string `json:"Id"`
}

//上传的信息
type UploadStoreInfo struct {
	ID       string `json:"Id"`
	IP       string `json:"Ip"`
	HttpPort string `json:"HttpPort"`
	TcpPort  string `json:"TcpPort"`
	NodeId   string `json:"NodeId"`
	Weight   int    `json:"Weight"`
}

type SyncPercent struct {
	TaskId   int64  `json:"taskId"`
	Percent  int    `json:"percent"`
	TargetId string `json:"targetId"`
	StoreId  string `json:"storeId"`
	RecordId int64  `json:"recordId"`
}

type SyncFullCmd struct {
	TaskId   int64  `json:"taskId"`
	RecordId int64  `json:"recordId"`
	Ip       string `json:"ip"`
	Port     string `json:"port"`
}

type ConnTcpServerCmd struct {
	DbPort   int    `json:"dbPort"`
	FilePort int    `json:"filePort"`
	TaskId   int64  `json:"taskId"`
	IP       string `json:"Ip"`
}

type Response struct {
	SerialNum uint64 //帧流水号
	Cmd       int    //命令
	Data      []byte //数据
}

func NewResponse() *Response {
	return new(Response)
}

type DataResult struct {
	Code string `json:"code"`
	Msg  string `json:"msg"`
}

func GetResult(code, msg string) []byte {
	var result DataResult
	result.Code = code
	result.Msg = msg
	r, err := json.Marshal(result)
	if err != nil {
		return nil
	}
	return r
}

func GetResponse(cmdstr string, serialnum uint64, code, msg string) *Response {
	cmd, _ := strconv.Atoi(cmdstr)
	resp := &Response{
		SerialNum: serialnum,
		Cmd:       cmd,
		Data:      GetResult(code, msg),
	}
	return resp
}

type SyncDb struct {
	StoreId string   `json:"storeId"`
	Total   int      `json:"total"`
	TaskId  int64    `json:"taskId"`
	Data    SyncData `json:"data"`
}

type SyncData struct {
	ID               int64  `json:"Id"`
	FileKey          string `json:"fileKey"`
	FileName         string `json:"fileName"`
	FileOriginalName string `json:"fileOriginalName"`
	FileSuffixName   string `json:"fileSuffixName"`
	FileSize         string `json:"fileSize"`
	FileStatus       string `json:"fileStatus"`
	Content          string `json:"content"` //存放目录
	FileByte         []byte `json:"fileByte"`
	PackNo           int    `json:"packNo"` //分包索引
	Pack             int    `json:"pack"`   //分包数量
}

type UpdateSync struct {
	FileKey string `json:"fileKey"`
}

//type UploadSyncFile struct {
//	NodeID           string `json:"nodeId"`
//	StoreId          string `json:"storeId"`
//	FileKey          string `json:"fileKey"`
//	FileName         string `json:"fileName"`
//	FileOriginalName string `json:"fileOriginalName"`
//	FileSuffixName   string `json:"fileSuffixName"`
//	FileSize         string `json:"fileSize"`
//	Content          string `json:"content"` //存放目录
//	FileByte         []byte `json:"fileByte"`
//	PackNo           int    `json:"packNo"` //分包索引
//	Pack             int    `json:"pack"`   //分包数量
//}

var nodeconf = &NoticeNodeConf{
	NodeID:       "",
	NoticeStores: nil,
}

func GetNodeConf() *NoticeNodeConf {
	return nodeconf
}

func SetNodeConf(nodeid string, ns []NoticeStore) {
	nodeconf.NoticeStores = ns
	nodeconf.NodeID = nodeid
}

type NoticeNodeConf struct {
	NodeID       string        `json:"nodeId"`
	NoticeStores []NoticeStore `json:"noticeStores"`
}

type NoticeStore struct {
	StoreId  string `json:"storeId"`
	StoreIp  string `json:"storeIp"`
	HttpPort string `json:"httpPort"`
}

type TranmitServerCmd struct {
	DbPort int   `json:"dbPort"`
	TaskId int64 `json:"taskId"`
}

type TranmitServerResultCmd struct {
	Code   string `json:"code"`
	Msg    string `json:"msg"`
	TaskId int64  `json:"taskId"`
}

type CloseTcpTun struct {
	TaskId   int64  `json:"taskId"`
	TargetId string `json:"targetId"`
	StoreId  string `json:"storeId"`
}

type UploadHardWareInfo struct {
	StoreId        string  `json:"storeId"`
	CpuPercent     float64 `json:"cpuPercent"`
	CpuCount       int     `json:"cpuCount"`
	CpuGhz         float64 `json:"cpuGhz"`
	DiskPercent    float64 `json:"diskPercent"`
	DiskTotal      int64   `json:"diskTotal"`
	DiskUsed       int64   `json:"diskUsed"`
	SwapTotal      int64   `json:"swapTotal"`
	SwapUsed       int64   `json:"swapUsed"`
	SwapPercent    float64 `json:"swapPercent"`
	MemPercent     float64 `json:"memPercent"`
	MemTotal       int64   `json:"memTotal"`
	MemUsed        int64   `json:"memUsed"`
	StoreFileCount int64   `json:"storeFileCount"`
}
