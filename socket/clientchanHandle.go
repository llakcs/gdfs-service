package socket

import "gdfs-service/logcat"

//客户端处理
func ClientChannelHandler(c *TcpClient) {
	//解码
	data := DecoderData(c.ByteBuffer)
	var resp *Response
	if data != nil {
		logcat.Info("###收到命令:", data.Cmd)
		switch data.Cmd {
		case "09": //收到创建文件传输TCP服务端指令
			AnalysisCreateTcpServer(data)
		case "04": //收到停止同步指令
			resp = AnalysisStopFullDataSync(data)
		case "07": //收到连接文件传输TCP服务端指令
			AnalysisConnTcpServer(data)
		case "03": //收到关闭文件传输TCP通道指令
			AnalysisCloseTcpTun(data)
		case "0b": //收到节点绑定或者解绑通知
			AnalysisNoticeNode(data)
		}
		if resp != nil {
			c.Conn.Write(EncoderData(resp))
		}
	}

}

func ControllerHandler(handler *ChannelInboundHandler) {
	data := DecoderData(handler.ByteBuffer)
	var resp *Response
	if data != nil {
		switch data.Cmd {
		case "08": //收到数据传输指令
			AnalysisDbInfo(data)
		}
		if resp != nil {
			handler.Conn.Write(EncoderData(resp))
		}
	}
}
