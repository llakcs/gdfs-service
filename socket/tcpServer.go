package socket

import (
	"gdfs-service/logcat"
	"log"
	"net"
	"time"
)

var tcplistens = make([]*net.TCPListener, 0)

type TcpServer struct {
	ChannelFutures []*ChannelFuture //socket通道切片
	Address        string           //监听IP地址
}

func NewSocket() *TcpServer {
	return &TcpServer{
		ChannelFutures: make([]*ChannelFuture, 0),
	}
}

type ReadLenError struct {
}

func (r *ReadLenError) Error() string {
	return "读取内容长度为0"
}

//通道
type ChannelFuture struct {
	Port           string        //监听端口
	ReadTimeout    time.Duration //服务端读取不到数据超时时间
	BufferSize     int           //缓冲区大小
	ChannelHandler channelHandler
}

//通道入站处理程序
type ChannelInboundHandler struct {
	ByteBuffer []byte
	Conn       net.Conn
}

type channelHandler func(c *ChannelInboundHandler)

func (s *TcpServer) MakeTcpChannel(port string, bufferSize int, readTimeout time.Duration, handler channelHandler) *ChannelFuture {
	return &ChannelFuture{
		Port:           port,
		ReadTimeout:    readTimeout,
		BufferSize:     bufferSize,
		ChannelHandler: handler,
	}
}

func newChannelInboundHandler(buf []byte, conn net.Conn) *ChannelInboundHandler {
	return &ChannelInboundHandler{
		ByteBuffer: buf,
		Conn:       conn,
	}
}

//绑定服务
func (s *TcpServer) BindServer(channels ...*ChannelFuture) {
	for _, channel := range channels {
		s.ChannelFutures = append(s.ChannelFutures, channel)
	}

}

func listenchan() {
	go func() {
		for {
			select {
			case <-StopTcpListenChan:
				for _, l := range tcplistens {
					if err := l.Close(); err != nil {
						logcat.Info("###关闭传输文件tcp监听")
					}
				}
			}
		}
	}()
}

//设置TCP监听地址
func (s *TcpServer) ListenTcp() {
	listenchan()
	log.Println("###开始监听")
	for _, channel := range s.ChannelFutures {
		go func(channel *ChannelFuture) {
			log.Println("###监听端口:" + channel.Port)
			addr, _ := net.ResolveTCPAddr("tcp4", s.Address+":"+channel.Port)
			listener, _ := net.ListenTCP("tcp", addr)
			tcplistens = append(tcplistens, listener)
			for {
				conn, err := listener.Accept()
				if err != nil {
					continue
				}
				go handlerClient(conn, channel)
			}
		}(channel)
	}
	//select {}
}

//处理客户端消息
func handlerClient(conn net.Conn, c *ChannelFuture) {

	buf := make([]byte, c.BufferSize) //设置读取数据缓冲区
	defer conn.Close()                // close connection before exit
	for {
		if c.ReadTimeout != 0 {
			conn.SetReadDeadline(time.Now().Add(c.ReadTimeout)) //设置连接超时时间
		}
		read_len, connerr := conn.Read(buf) //读取客户端发来的数据
		var readlenerr error
		if read_len == 0 {
			readlenerr = &ReadLenError{}
		}
		if connerr != nil || readlenerr != nil {
			logcat.Info("connerr：", connerr, " readlenerr:", readlenerr)
			return
		}
		//把数据回调到handler作处理
		go c.ChannelHandler(newChannelInboundHandler(buf[:read_len], conn))
		////转成16进制string
		//result := hex.EncodeToString(buf[:read_len])
		//log.Println(result) //打印数据
		//conn.Write(buf[:read_len])
	}
}
