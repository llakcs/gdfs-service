package socket

var StopFUllSyncChan = make(chan bool)
var ConnTcpChan = make(chan ConnTcpServerCmd)
var SyncDbChan = make(chan SyncDb)
var CreateTcpServerChan = make(chan TranmitServerCmd)
var StopTcpListenChan = make(chan bool)
var StopConnTcpChan = make(chan bool)
var SyncUpdateChan = make(chan UpdateSync, 999)
