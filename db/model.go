package db

import "time"

type FileInfo struct {
	Id               int64  `gorm:"column:id;AUTO_INCREMENT;primary_key"`
	FileKey          string `gorm:"column:file_key;type:varchar(255);not null;index:idx_fileinfo_file_key,unique;comment:文件秘钥"`
	FileName         string `gorm:"column:file_name;type:varchar(255);comment:文件本地存储名"`
	FileOriginalName string `gorm:"column:file_original_name;type:varchar(255);comment:文件原名"`
	FileSuffixName   string `gorm:"column:file_suffix_name;type:varchar(255);comment:文件后缀"`
	FilePath         string `gorm:"column:file_path;type:varchar(255);comment:文件存储相对路径"`
	FileSize         string `gorm:"column:file_size;type:varchar(32);comment:文件大小"`
	SyncStatus       string `gorm:"column:sync_status;type:varchar(1);comment:文件同步状态 y已同步 n未同步"`
	CreatedAt        time.Time
	UpdatedAt        time.Time
}

type StoreInfo struct {
	Id        int64  `gorm:"column:id;AUTO_INCREMENT;primary_key"`
	StoreId   string `gorm:"column:store_id;type:varchar(255)"`
	NodeID    string `gorm:"column:node_id;type:varchar(255)"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (StoreInfo) TableName() string {
	return "gdfs_storeinfo"
}

//设置表名接口
func (FileInfo) TableName() string {
	return "gdfs_fileinfo"
}
